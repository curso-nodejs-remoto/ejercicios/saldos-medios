## Ejercicio - Saldos medios

1. Tenemos un array de clientes de un banco. De cada cliente tenemos el DNI, el nombre y los apellidos, el saldo medio anual, el saldo máximo anual, si tiene la nómina domiciliada o no y sus teléfonos de contacto.
2. Crea un archivo app.js que importe la información de `clientes.json`.
3. Mediante un bucle for...of, imprime por consola todos los DNIs de los clientes que tengan teléfono fijo.
4. Crea una función que reciba el array inicial de clientes y devuelva un nuevo array de clientes, donde cada cliente tenga la siguiente forma y donde sólo estén los clientes con nómina y con un saldo medio menor a 2000€ (usa una constante para almacenar este valor al principio del script):
```
    {
        dni: "XXXXXXXXX",
        nombreApellidos: "XXXXX XXXXXX XXXXXX",
        saldoMedio: X
    }
```
5. Crea una función que reciba un array de clientes (ponle como valor por defecto un array vacío) con la forma del punto anterior y que devuelva la media de todos los saldos medios obtenidos.
6. Utilizando dicha función y la constante definida en el punto 4, imprime por consola un mensaje como éste utilizando template literals:
"El saldo medio promedio de los clientes con nómina y con saldo medio menor a X€ es: Y€".
7. Nuestra aplicación va a empezar a funcionar en EEUU y necesita mostrar dólares o euros según dónde esté instalada. Crea un archivo de configuración `.env` que contenga una variable de entorno con el país (puede tener dos valores: `ES` o `US`). Dependiendo del país, la frase del punto 6 deberá mostrar el símbolo del euro o el del dólar.